﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    //this will run the game and create the different cards 
    /// making a grid 
    public const int gridRows = 2;
    public const int gridColums = 4;
    public const int offsetX = 4;
    public const int offsetY = 5;

    [SerializeField] private mainCard originalCard;
    [SerializeField] private Sprite[] images;

    private void Start()
    {
        Vector3 startPos = originalCard.transform.position;
        // gets the position of the first card || this will get the other cards to offset from this position

        int[] numbers = { 0, 0, 1, 1, 2, 2, 3, 3 };
        numbers = ShuffleArray(numbers);

        for (int i = 0; i < gridColums; i++)
        {
            for (int j = 0; j < gridRows; j++)
            {

                mainCard card;
                if (i == 0 && j == 0)
                {
                    card = originalCard;

                }
                else
                {
                    card = Instantiate(originalCard) as mainCard;
                }
                int Index = j * gridColums + i;
                int id = numbers[Index];
                card.ChangingSprite(id, images[id]);
                // this will set the image andId to the display
                // then use the

                float posX = (offsetX * i) + startPos.x;
                float posY = (offsetY * j) + startPos.y;

                card.transform.position = new Vector3(posX, posY, startPos.z);
            }


        }
    }
    // shuffles the array and returns it in new order 
    private int[] ShuffleArray(int[] numbers)
    {

        int[] newArray = numbers.Clone() as int[];
        for (int i = 0; i < newArray.Length; i++)
        {

            int temp = newArray[i];
            int r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];

            newArray[r] = temp;
        }
        return newArray;
    }
    private mainCard _firstReveal;
    private mainCard _secondReveal;

    private int _score = 0;
    [SerializeField] private TextMesh scoreLabel;

    public bool CanReveal
    {

        get { return _secondReveal == null; }

    }
    public void cardRevealed(mainCard card)
    {
        if (_firstReveal == null)
        {

            _firstReveal = card;

        }
        else
        {
            _secondReveal = card;
            StartCoroutine(CheckMatch());


        }




    }
    private IEnumerator CheckMatch()
    {
        if (_firstReveal.id == _secondReveal.id)
        {// if the cards match 
            _score++;
            scoreLabel.text = "Score:" + _score;

        }
        else
        {

            //cover the slected cards again
            yield return new WaitForSeconds(0.5f);
            _firstReveal.Unreveal();
            _secondReveal.Unreveal();


        }

        _firstReveal = null;
        _secondReveal = null;
    }
}
