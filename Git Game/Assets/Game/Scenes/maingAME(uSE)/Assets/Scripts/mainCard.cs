﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mainCard : MonoBehaviour
{
    [SerializeField] private SceneController controller;
    [SerializeField] private GameObject Card_Back;
    // serialized field makes private values visible in inspector > unity API

    public void OnMouseDown()
    {
        // STEP 1:  make the back of card reveal card underneath
        if (Card_Back.activeSelf && controller.CanReveal)
        {
            Card_Back.SetActive(false);
            // this will check  if we click on the card , if back of card is active 
            //make inactive to reveal card underneath ( the main card)
            controller.cardRevealed(this);


        }

        //  STEP 2: now randomize the card 
    }
    private int _id;
    public int id
    {
        get { return _id; }
    }
       public void ChangingSprite( int id,Sprite Image)
    {
        _id = id;
        GetComponent<SpriteRenderer>().sprite = Image;

        //get the sprite component and changes it's property


    }
    public void Unreveal()
    {
        Card_Back.SetActive(true);
        //this will cover the card again 

    }
}

