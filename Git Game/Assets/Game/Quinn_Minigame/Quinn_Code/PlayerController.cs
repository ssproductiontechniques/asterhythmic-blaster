﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float maxSpeed;
    public float jumpHeight;
    public int health;
    bool jump;

    public bool onFloor;

    private Rigidbody2D rb;
    private Animator anim;
    private SpriteRenderer sRend;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sRend = GetComponent<SpriteRenderer>();
        health = 3;
    }

    private void Update()
    {

        anim.SetBool("grounded", onFloor);
        anim.SetFloat("x", Mathf.Abs(rb.velocity.x));

        if (Input.GetAxis("Horizontal") < -.1f)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        if (Input.GetAxis("Horizontal") > .1f)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
       jump = Input.GetKeyDown(KeyCode.Space);
       
        if (health <= 0)
        {
            sRend.enabled = false;
            Destroy(GameObject.Find("gun"));
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 easeVelocity = rb.velocity;
        easeVelocity.y = rb.velocity.y;
        easeVelocity.x *= .75f;
        easeVelocity.z = 0f;



        float moveHorizontal = Input.GetAxisRaw("Horizontal");

        if (jump & onFloor)
        {
            onFloor = false;
            rb.AddForce(Vector2.up * jumpHeight);
        }




        if (onFloor)
        {
            rb.velocity = easeVelocity;

            Vector3 movement = new Vector3(moveHorizontal, 0, 0);
            rb.AddForce(movement * speed);
            if (rb.velocity.x > maxSpeed)
            {
                rb.velocity = new Vector2(maxSpeed, rb.velocity.y);
            }
            if (rb.velocity.x < -maxSpeed)
            {
                rb.velocity = new Vector2(-maxSpeed, rb.velocity.y);
            }
        }
        //else if (onFloor == false)
        // {
        // Vector3 movement = new Vector3(moveHorizontal, 0, 0);
        // rb.AddForce(movement * 3);
        //}
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "health" && health < 3)
        {
            health += 1;
            Destroy(col.gameObject);
        }


    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "platform" || col.gameObject.tag == "enemy")
        {
            onFloor = true;
        }

        if (col.gameObject.tag == "spikes" || col.gameObject.tag == "enemy")
        {
            health -= 1;

            if (!onFloor)
            {
                float moveHorizontal = Input.GetAxisRaw("Horizontal");
                Vector3 knockback = new Vector3(moveHorizontal * 2, 12, 0);
                rb.AddForce(knockback * 5);
            }
            else if (onFloor)
            {
                float moveHorizontal = Input.GetAxisRaw("Horizontal");
                Vector3 knockback = new Vector3(moveHorizontal * -20, 0, 0);
                rb.AddForce(knockback * 5);
            }
        }
    }
}
