﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject target;
    private Transform pos;
    float lookDist;
    float smooth;

    // Use this for initialization
    void Start()
    {
        pos = target.transform;
        lookDist = 0;
        smooth = .125f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        Vector3 endPos = new Vector3(pos.position.x + lookDist, pos.position.y, -10);
        Vector3 smoothedPos = Vector3.Lerp(transform.position, endPos, smooth);
        transform.position = smoothedPos;

        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            lookDist = .2f;
        }
        else if (Input.GetAxisRaw("Horizontal") < 0)
        {
            lookDist = -.2f;
        }
    }
}
