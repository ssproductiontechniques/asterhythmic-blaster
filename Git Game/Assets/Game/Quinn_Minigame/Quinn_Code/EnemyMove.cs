﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{

    Rigidbody2D rb;
    public float speed;
    float currentDirection;
    float health;

    bool onFloor;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        onFloor = false;
        health = 2;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        Vector3 easeVelocity = rb.velocity;
        easeVelocity.y = rb.velocity.y;
        easeVelocity.x *= .75f;
        easeVelocity.z = 0f;
        rb.velocity = easeVelocity;

        currentDirection = transform.localScale.x;

        if (health <= 0)
        {
            Destroy(gameObject);
        }

        if (onFloor)
        {

            rb.AddForce(new Vector2(speed * currentDirection, 0));

        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "bullet")
        {
            health -= 1;
        }
        if (col.gameObject.tag == "platform")
        {
            onFloor = true;
        }
        if (col.gameObject.tag == "enemy")
        {
            transform.localScale = new Vector3(currentDirection * -1, 1, 1);
        }
        if (col.gameObject.tag == "spikes")
        {
            Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), col.gameObject.GetComponent<Collider2D>());
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "barrier")
        {
            transform.localScale = new Vector3(currentDirection * -1, 1, 1);
        }

        if (col.name == "LeftSpawnTop")
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (col.name == "RightSpawnTop")
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (col.name == "LeftSpawnBottom")
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (col.name == "RightSpawnBottom")
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

    }
}
