﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private GameObject player;

    Rigidbody2D rb;
    float direction;
    public float speed;
    float setDir;

    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player");
        direction = player.GetComponent<Transform>().transform.localScale.x;
        rb = GetComponent<Rigidbody2D>();
        transform.Rotate(new Vector3(0, 0, Mathf.Floor(Random.Range(-2f, 2f))));
        setDir = direction;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(new Vector2(speed * setDir, 0), ForceMode2D.Impulse);
        //transform.Translate(new Vector3(speed * setDir, 0) * Time.deltaTime);
        Destroy(gameObject, .5f);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        Destroy(gameObject);
    }
}
