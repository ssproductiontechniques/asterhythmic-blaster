﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public GameObject Player;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0) && Mathf.Abs(Player.GetComponent<Rigidbody2D>().velocity.x) < 0.1f)
        {
            StartCoroutine(Shake(.15f, .009f));
        }
    }

    public IEnumerator Shake(float dur, float power)
    {
        Vector3 origonalPos = transform.position;

        float elapsed = 0f;

        while (elapsed < dur)
        {
            float x = Random.Range(-1f, 1f) * power;
            float y = Random.Range(-1f, 1f) * power;

            transform.localPosition = new Vector3(x + transform.position.x, y + transform.position.y, origonalPos.z);

            elapsed += Time.deltaTime;

            yield return null;
        }
        transform.position = origonalPos;
    }
}
