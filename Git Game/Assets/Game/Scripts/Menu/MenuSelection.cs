﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSelection : MonoBehaviour
{
    public int sceneToLoad;

    private void OnMouseOver()
    {
        gameObject.ShakePosition(new Vector3(0.1f, 0.1f, 0f), 0.1f, 0f);
    }

    private void OnMouseDown()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
