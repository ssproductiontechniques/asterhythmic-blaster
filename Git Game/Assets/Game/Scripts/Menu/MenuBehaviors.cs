﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBehaviors : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        Exit();
    }

    private void Exit()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
