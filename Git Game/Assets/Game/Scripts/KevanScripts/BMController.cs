﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BMController : MonoBehaviour
{
    public float mashVariable;
    public float mashVariableMax;

    public float mashGoal;
    public float mashGoalWidth;

    public float mashIncreaseRate;
    public float mashDecreaseRate;

    public float roundTimer;
    public float roundTimerMax;
    public float initialRoundTimerMax;

    public float roundNumber;
    public float roundNumberHighScore;

    public Canvas canvasRef;
    public Text roundText;
    public Text highScoreText;
    public Text timerText;

    void Start()
    {
        StartNewLevel();
        initialRoundTimerMax = roundTimerMax;
    }

    private void FixedUpdate()
    {
        
    }

    void Update()
    {
        MashOnButtonHit();
        MashAtrophy();
        ManageRounds();
        UpdateUI();
    }

    void MashOnButtonHit()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            mashVariable += mashIncreaseRate;
        }
    }

    void MashAtrophy()
    {
        if (mashVariable > 0)
        {
            mashVariable -= mashDecreaseRate;
        }
        else if (mashVariable < 0)
        {
            mashVariable = 0;
        }
        else if (mashVariable > mashVariableMax)
        {
            mashVariable = mashVariableMax;
        }
    }

    void ManageRounds()
    {
        if (roundTimer <= 0)
        {
            if (mashVariable > mashGoal - (mashGoalWidth/2) && mashVariable < mashGoal + (mashGoalWidth / 2))
            {
                StartNewLevel();
            }
            else
            {
                roundNumberHighScore = roundNumber;
                SceneManager.LoadScene(1);
            }
        }

        roundTimer = roundTimer - Time.deltaTime;
    }

    void StartNewLevel()
    {
        roundNumber++;

        if (mashGoalWidth > 5)
        {
            mashGoalWidth = mashGoalWidth - 0.5f;
        }
        else if (mashGoalWidth == 5 && roundTimerMax != 5)
        {
            roundTimerMax = initialRoundTimerMax - ((roundNumber - 1) * 0.5f);
        }
        else if (roundTimerMax == 5)
        {
            return;
        }

        roundTimer = roundTimerMax;

        //Change mashGoal to a new value every new round
        mashGoal = Random.Range(0 + (mashGoalWidth/2), 100 - (mashGoalWidth/2));
    }

    void UpdateUI()
    {
        roundText.text = "Round: " + roundNumber;
        highScoreText.text = "Highest Round: " + roundNumberHighScore;
        timerText.text = "" + roundTimer;
    }
}