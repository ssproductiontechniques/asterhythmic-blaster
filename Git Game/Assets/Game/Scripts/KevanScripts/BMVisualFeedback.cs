﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BMVisualFeedback : MonoBehaviour
{
    public GameObject meterBar;
    public GameObject mashGoalObject;
    public GameObject mashMarkerObject;

    public Text arrowText;

    public BMController controllerScriptRef;

    public float mashGoalPosition;
    public float mashGoalAdaptedScale;

    public float mashMarkerPosition;

    public Color mashGoalColor;
    public Color mashGoalSafeColor;

    public Color arrowTextColor;
    public Color arrowTextMinColor;
    public Color arrowTextMaxColor;

    void Start()
    {
        controllerScriptRef = gameObject.GetComponent<BMController>();
    }

    void Update()
    {
        MashGoalVisuals();
        MashMarkerControl();
    }

    void MashGoalVisuals()
    {
        mashGoalAdaptedScale = controllerScriptRef.mashGoalWidth / 100;

        mashGoalPosition = Mathf.Lerp(transform.position.y - 3f, transform.position.y + 3f, controllerScriptRef.mashGoal / 100);
        mashGoalObject.transform.position = new Vector3(0, mashGoalPosition, 0);
        mashGoalObject.transform.localScale = new Vector3(1, mashGoalAdaptedScale, 1);

        if (controllerScriptRef.mashVariable > controllerScriptRef.mashGoal - (controllerScriptRef.mashGoalWidth / 2) && controllerScriptRef.mashVariable < controllerScriptRef.mashGoal + (controllerScriptRef.mashGoalWidth / 2))
        {
            mashGoalObject.gameObject.ColorUpdate(mashGoalSafeColor, 0);
        }
        else
        {
            mashGoalObject.gameObject.ColorUpdate(mashGoalColor, 0);
        }
    }

    void MashMarkerControl()
    {
        mashMarkerPosition = Mathf.Lerp(transform.position.y - 3.2f, transform.position.y + 2.7f, controllerScriptRef.mashVariable / 100);
        mashMarkerObject.transform.position = new Vector3(0.75f, mashMarkerPosition, 0);

        arrowTextColor = Color.Lerp(arrowTextMinColor, arrowTextMaxColor, controllerScriptRef.mashVariable / 100);
        arrowText.color = arrowTextColor;

        iTween.ShakePosition(Camera.main.gameObject, new Vector3(controllerScriptRef.mashVariable / 5000, 0, 0), 0.1f);
    }
}