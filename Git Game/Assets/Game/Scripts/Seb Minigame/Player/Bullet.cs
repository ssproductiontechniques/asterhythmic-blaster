﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        Invoke("DieAfterTime", 10f);
    }

    private void Update()
    {
        HandleBounds();
    }

    public void Fire(Vector3 direction)
    {
        rb.velocity = direction * speed;
    }

    private void HandleBounds()
    {
        if(transform.position.magnitude > 50f)
        {
            Destroy(this.gameObject);
        }
    }

    private void DieAfterTime()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
    }
}
