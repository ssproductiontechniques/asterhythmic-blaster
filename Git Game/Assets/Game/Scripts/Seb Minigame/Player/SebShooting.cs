﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SebShooting : MonoBehaviour
{
    public GameObject bullet;

    public Transform bulletSpawn;

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    private void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/Seb Minigame/Shoot").start();
            Camera.main.gameObject.ShakePosition(new Vector3(0.1f, 0.1f, 0f), 0.25f, 0f);
            GameObject instBullet = Instantiate(bullet);
            instBullet.transform.position = bulletSpawn.position;
            instBullet.GetComponent<Bullet>().Fire(transform.up);
        }
    }
}
