﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FollowMouse : MonoBehaviour {

    public bool canMove = true;

    private Vector3 previousPositon;

    private FMOD.Studio.EventInstance engine;
    private FMOD.Studio.ParameterInstance speedParam;

    private void Start()
    {
        HandleCursor.HandleCursorVisibility(false);
        engine = FMODUnity.RuntimeManager.CreateInstance("event:/Seb Minigame/Engine");
        engine.getParameter("Speed", out speedParam);
        engine.start();
    }

    // Update is called once per frame
    void Update () {


        Move(); 
        RotateToMouse();

        float speedValue;

        speedParam.getValue(out speedValue);

        

        speedParam.setValue(Mathf.Lerp(speedValue, (transform.position - previousPositon).magnitude / Time.deltaTime / 20f, 0.1f));

        Debug.Log(((transform.position - previousPositon).magnitude / Time.deltaTime) / 20f);

    }

    private void LateUpdate()
    {
        previousPositon = transform.position;
    }

    private void Move()
    {
        if (canMove)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(GetMousePositionInWorldSpace.GetMousePosition().x, GetMousePositionInWorldSpace.GetMousePosition().y, transform.position.z), 0.25f);
        }

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -9f, 9f), Mathf.Clamp(transform.position.y, -5f, 5f), transform.position.z);
    }

    private void RotateToMouse()
    {
        transform.rotation = Quaternion.LookRotation(transform.forward, GetMousePositionInWorldSpace.GetMousePosition() - transform.position);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Obstacle")
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/Seb Minigame/Die").start();
            SceneManager.LoadScene(1);
        }
    }

    private void OnDestroy()
    {
        HandleCursor.HandleCursorVisibility(true);
    }
}
