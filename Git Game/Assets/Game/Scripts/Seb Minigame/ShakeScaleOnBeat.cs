﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeScaleOnBeat : MonoBehaviour
{
    public float bpm;

    private float spb;
    
    private void Start()
    {
        SetSPB();
        StartCoroutine(HandleBeat());
    }
    private void SetSPB()
    {
        spb = 60f / bpm;
    }

    private IEnumerator HandleBeat()
    {
        yield return new WaitForSeconds(spb);
        while (true)
        {
            this.gameObject.ShakeScale(new Vector3(0.1f, 0.1f, 0f), 0.2f, 0f);
            yield return new WaitForSeconds(spb);
        }
    }
}
