﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float minSpeed, maxSpeed;

    public float minScale, maxScale;

    private float speed;

    private Vector3 direction;

    private FollowMouse player;

    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<FollowMouse>();
    }

    // Start is called before the first frame update
    void Start()
    {
        float selectedScale = Random.Range(minScale, maxScale);
        transform.localScale = new Vector3(selectedScale, selectedScale, 1f);

        speed = Random.Range(minSpeed, maxSpeed);
        direction = (player.transform.position - transform.position).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = direction * speed;

        if(transform.position.magnitude > 25f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/Seb Minigame/Explode").start();
            Camera.main.gameObject.ShakePosition(new Vector3(0.2f, 0.2f), 0.25f, 0f);
            FindObjectOfType<ScoreManager>().score++;
            Destroy(this.gameObject);
        }
    }
}
