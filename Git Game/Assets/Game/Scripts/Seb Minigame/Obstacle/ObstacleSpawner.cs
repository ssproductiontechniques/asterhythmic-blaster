﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public GameObject obstacle;

    public float bpm;

    private float spb;

    // Start is called before the first frame update
    void Start()
    {
        SetSPB();
        StartCoroutine(SpawnObstacle());   
    }

    private void SetSPB()
    {
        spb = 60f / bpm;
    }

    private IEnumerator SpawnObstacle()
    {
        yield return new WaitForSeconds(spb);
        while (true)
        {
            bool xSide = (Random.value > 0.5f);
            bool ySide = (Random.value > 0.5f);

            Vector3 spawnPoint = Vector3.zero;

            if (!ySide)
            {
                if (xSide)
                {
                    spawnPoint.x = -10f;
                    spawnPoint.y = Random.Range(-5f, 5f);
                }
                else
                {
                    spawnPoint.x = 10f;
                    spawnPoint.y = Random.Range(-5f, 5f);
                }
            }
            else
            {
                if (xSide)
                {
                    spawnPoint.x = Random.Range(-10f, 10f);
                    spawnPoint.y = 7f;
                }
                else
                {
                    spawnPoint.x = Random.Range(-10f, 10f);
                    spawnPoint.y = -7f;
                }
            }

            GameObject instObstacle = Instantiate(obstacle);
            instObstacle.transform.position = spawnPoint;
            yield return new WaitForSeconds(spb);
        }
    }


}
