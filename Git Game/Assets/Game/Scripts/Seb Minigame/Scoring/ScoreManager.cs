﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{ 
    public Text scoreDisplay;

    public int score;

    // Update is called once per frame
    void Update()
    {
        CheckScore();
        UpdateText();
    }

    private void CheckScore()
    {
        if(score > PlayerPrefs.GetInt("highscore"))
        {
            PlayerPrefs.SetInt("highscore", score);
        }
    }

    private void UpdateText()
    {
        scoreDisplay.text = score.ToString() + " | " + PlayerPrefs.GetInt("highscore").ToString();
    }
}
